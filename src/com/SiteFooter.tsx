import React from 'react';

const rawYear = new Date().getFullYear();

export const SiteFooter: React.FC = () => (
    <footer>
        Copyright &copy; Alejandro A. Shevyakov, 2020-{rawYear}
    </footer>
);