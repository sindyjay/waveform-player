import React from 'react';

export const SiteHeader = () => (
    <header>
        <h1>Waveform Player</h1>
    </header>
);