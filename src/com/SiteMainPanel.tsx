import React from 'react';
import { PlayerWindow } from './PlayerWindow';

export const SiteMainPanel: React.FC = () => (
    <main>
        <PlayerWindow></PlayerWindow>
    </main>
);