import React from 'react';

export const SiteNavigation: React.FC = () => (
    <nav>
        <ul></ul>
        <a href="mailto:sindyjay@yandex.ru">sindyjay@yandex.ru</a>
    </nav>
);