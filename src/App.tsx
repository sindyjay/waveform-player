import React from 'react';

import { SiteFooter } from './com/SiteFooter';
import { SiteHeader } from './com/SiteHeader';
import { SiteMainPanel } from './com/SiteMainPanel';
import { SiteNavigation } from './com/SiteNavigation';

const App: React.FC = () => {
  return <>
    <SiteHeader></SiteHeader>
    <SiteNavigation></SiteNavigation>
    <SiteMainPanel></SiteMainPanel>
    <SiteFooter></SiteFooter>
  </>
}

export default App;
