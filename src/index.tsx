import React from 'react';
import ReactDOM from 'react-dom';
import './wp.min.css';
import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  // document.getElementsByTagName('body')
  document.getElementById('WPApplication')
);
