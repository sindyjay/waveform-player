# Это помойка с заготовками, которые могут попасть или не попасть в документацию

Если надо будет обновить NodeJS, необходимо выполнить следующее:
```bash
npm cache clean -f
sudo npm install -g n
sudo n stable
```
Также можно ставить не последнюю стабильную версию, а самую свежую. Тогда последняя строка должны быть `sudo n latest`. Если же нужна какая-то конкретная версия, то `sudo n [version.number]`.

## index.tsx
```tsx
// последняя директива импорта вначале
import reportWebVitals from './reportWebVitals';

// это в самом конце файла
// If you want to start measuring performance in your app, pass a function to log results (for example: reportWebVitals(console.log)) or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
```

## index.html
```html
<!-- перед manifest.json -->
<link rel="apple-touch-icon" href="%PUBLIC_URL%/logo192.png" />
<!--
    manifest.json provides metadata used when your web app is installed on a
    user's mobile device or desktop. See https://developers.google.com/web/fundamentals/web-app-manifest/
-->
<!-- перед title -->
<!--
    Notice the use of %PUBLIC_URL% in the tags above.
    It will be replaced with the URL of the `public` folder during the build.
    Only files inside the `public` folder can be referenced from the HTML.

    Unlike "/favicon.ico" or "favicon.ico", "%PUBLIC_URL%/favicon.ico" will
    work correctly both with client-side routing and a non-root public URL.
    Learn how to configure a non-root public URL by running `npm run build`.
-->
<!-- после div id="wp-app" -->
<!--
    This HTML file is a template.
    If you open it directly in the browser, you will see an empty page.

    You can add webfonts, meta tags, or analytics to this file.
    The build step will place the bundled scripts into the <body> tag.

    To begin the development, run `npm start` or `yarn start`.
    To create a production bundle, use `npm run build` or `yarn build`.
-->
```