const gulp = require('gulp');
const less = require('gulp-less');
const concat = require('gulp-concat');
const autoPrefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');

gulp.task('lessPreparer', function() {
    return gulp.src('./src/less/main.less')
        .pipe(less())
        .pipe(concat('wp.min.css'))
        .pipe(autoPrefixer({
            overrideBrowserslist: ['last 3 versions'],
            cascade: false
        }))
        .pipe(cleanCSS({
            compatibility: 'ie8',
            format: {
                semicolonAfterLastProperty: true
            }
        }))
        .pipe(gulp.dest('./src'));
});

gulp.task('watchStyles', function() {
    gulp.watch('./src/less/**/*', gulp.series(['lessPreparer']));
});

gulp.task('default', gulp.series(['watchStyles']));
